declare module 'react-social-media-icons';

interface SocialMediaIcons {
  url: string;
  className: string;
}
