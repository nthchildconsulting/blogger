import React, { useEffect } from 'react';
import ReactGA, { FieldsObject } from 'react-ga';

ReactGA.initialize('UA-147620744-1');

const withTracker = (WrappedComponent: any, options: FieldsObject = {}) => {
  const trackPage = (page: string) => {
    ReactGA.set({ page, ...options });
    ReactGA.pageview(page);
  };

  return (props: any) => {
    useEffect(() => {
      trackPage(props.location.pathname);
    }, [props.location.pathname]);

    return <WrappedComponent {...props} />;
  };
};

export default withTracker;
