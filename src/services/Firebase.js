import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const app = firebase.initializeApp({
  apiKey: 'AIzaSyC9ePkA5cGZ-77OzsxWyMqzjWRK5UO-lF4',
  authDomain: 'nthchildconsulting-blog.firebaseapp.com',
  databaseURL: 'https://nthchildconsulting-blog.firebaseio.com',
  projectId: 'nthchildconsulting-blog',
  storageBucket: 'nthchildconsulting-blog.appspot.com',
  messagingSenderId: '236120627077',
  appId: '1:236120627077:web:a8e87395955c17f673d6ec',
});

const db = firebase.firestore(app);

const adminLogin = (email, password) => {
  return firebase.auth().signInWithEmailAndPassword(email, password);
};

const login = () => {
  return firebase.auth().signInAnonymously();
};

const logout = async () => {
  await firebase.auth().signOut();
};

const listenForAuth = cb => {
  firebase.auth().onAuthStateChanged(cb);
};

const getAdminPosts = async () => {
  const snapshot = await db.collection('posts').get();
  if (!snapshot.empty) {
    const data = [];
    snapshot.forEach(doc => {
      data.push(doc.data());
    });
    return data;
  }
  return [];
};

const getPosts = async () => {
  const snapshot = await db
    .collection('posts')
    .where('publish', '==', true)
    .orderBy('pinned', 'desc')
    .get();
  if (!snapshot.empty) {
    const data = [];
    snapshot.forEach(doc => {
      data.push(doc.data());
    });
    return data;
  }
  return [];
};

const getPostsByTag = async tag => {
  const snapshot = await db
    .collection('posts')
    .where('tags', 'array-contains', tag)
    .where('publish', '==', true)
    .get();
  if (!snapshot.empty) {
    const data = [];
    snapshot.forEach(doc => {
      data.push(doc.data());
    });
    return data;
  }
  return [];
};

const getPostsByPopular = async () => {
  const snapshot = await db
    .collection('posts')
    .where('popular', '==', true)
    .where('publish', '==', true)
    .get();
  if (!snapshot.empty) {
    const data = [];
    snapshot.forEach(doc => {
      data.push(doc.data());
    });
    return data;
  }
  return [];
};

const getTags = async () => {
  const snapshot = await db
    .collection('posts')
    .where('publish', '==', true)
    .get();
  if (!snapshot.empty) {
    let data = [];
    snapshot.forEach(doc => {
      const docData = doc.data();
      data = data.concat(docData.tags);
    });
    return data;
  }
  return [];
};

const getPost = async id => {
  const doc = await db
    .collection('posts')
    .doc(id)
    .get();
  return doc.data();
};

const publishPost = async (id, publish) => {
  return db
    .collection('posts')
    .doc(id)
    .set(
      {
        publish,
      },
      { merge: true }
    );
};

function savePost(options) {
  const data = {
    ...options,
    createdAt: new Date().toISOString(),
    id: options.slug,
  };
  return db
    .collection('posts')
    .doc(data.id)
    .set(data, { merge: true });
}

const deletePost = async id => {
  return db
    .collection('posts')
    .doc(id)
    .delete();
};

export {
  adminLogin,
  login,
  logout,
  listenForAuth,
  savePost,
  getPost,
  getPosts,
  getPostsByTag,
  getTags,
  getPostsByPopular,
  publishPost,
  getAdminPosts,
  deletePost,
};
