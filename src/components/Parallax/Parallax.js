import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { fixedBackground } from 'assets/jss/mixins';

const useStyles = makeStyles(theme => ({
  root: props => ({
    textAlign: 'center',
    paddingTop: theme.spacing(1) * 5,
    paddingBottom: theme.spacing(1) * 10,
    ...fixedBackground(props.background),
    height: 400,
  }),
}));

const Parallax = props => {
  const classes = useStyles(props);
  return <div className={classes.root}></div>;
};

export default Parallax;
