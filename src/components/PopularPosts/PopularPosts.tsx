import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import SendIcon from '@material-ui/icons/Send';
import Divider from '@material-ui/core/Divider';
import SimplePaper from 'components/SimplePaper/SimplePaper';
import * as Firebase from 'services/Firebase';
import { useStore } from 'components/App/store/Context';

const PopularPost = withRouter(({ post, history }: any) => {
  return (
    <ListItem button onClick={() => history.push(`/post/${post.slug}`)}>
      <ListItemIcon>
        <SendIcon />
      </ListItemIcon>
      <ListItemText primary={post.title} secondary={post.tagline} />
    </ListItem>
  );
});

const PopularPosts: React.FC = () => {
  const [posts, setPosts] = useState<any>([]);
  const { user }: any = useStore();
  useEffect(() => {
    if (user) {
      const fetchPosts = async () => {
        const results = await Firebase.getPostsByPopular();
        setPosts(results);
      };
      fetchPosts();
    }
  }, [user]);
  return (
    <SimplePaper title="Popular Posts">
      <List>
        {posts.map((post: any, i: number) => {
          return (
            <div key={i}>
              <PopularPost post={post} />
              {i < posts.length - 1 && <Divider />}
            </div>
          );
        })}
      </List>
    </SimplePaper>
  );
};

export default PopularPosts;
