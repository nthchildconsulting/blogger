import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/styles';
import theme from 'themes/default';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import withTracker from 'hooks/withTracker';
import Home from 'views/Home/Home';
import BlogPost from 'views/Blog/Post';
import TagPosts from 'views/Tag/Posts';
import Admin from 'views/Admin/Admin';
import { withProvider } from './store/Context';

const App: React.FC = () => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Router>
        <Route path="/" exact component={Home} />
        <Route path="/admin" component={Admin} />
        <Route path="/post/:slug" exact component={withTracker(BlogPost)} />
        <Route path="/tag/:tag" exact component={withTracker(TagPosts)} />
      </Router>
    </ThemeProvider>
  );
};

export default withProvider(App);
