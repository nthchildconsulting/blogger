import React, { useState, useContext, useEffect } from 'react';
import * as Firebase from 'services/Firebase';

const Context = React.createContext([{}, () => {}]);

const Provider = props => {
  const [user, setUser] = useState({});
  useEffect(() => {
    Firebase.listenForAuth(user => {
      if (user && user.isAnonymous) {
        setUser(user);
      } else if (!user) {
        Firebase.login();
      }
    });
  }, []);
  return <Context.Provider value={{ user }}>{props.children}</Context.Provider>;
};

export const useStore = () => useContext(Context);

export function withProvider(Component) {
  return function WrapperComponent(props) {
    return (
      <Provider>
        <Component {...props} />
      </Provider>
    );
  };
}

export { Context, Provider };
