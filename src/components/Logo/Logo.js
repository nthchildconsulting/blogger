import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import logo from 'assets/img/logo.png';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(theme => ({
  logo: {
    width: 40,
  },
  text: props => ({
    color: props.light
      ? theme.palette.primary.contrastText
      : theme.palette.primary.main,
  }),
}));

const Logo = props => {
  const classes = useStyles(props);
  return (
    <Grid container alignItems="center" justify={props.justify || 'flex-start'}>
      <Box>
        <Link to="/">
          <img src={logo} alt="nth" className={classes.logo} />
        </Link>
      </Box>
      <Box m={1}>
        <Link to="/">
          <Typography
            variant="subtitle1"
            component="p"
            classes={{
              root: classes.text,
            }}
          >
            child consulting
          </Typography>
        </Link>
      </Box>
    </Grid>
  );
};

export default withRouter(Logo);
