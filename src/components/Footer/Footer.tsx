import React from 'react';
import { withRouter } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ScrollUpButton from 'react-scroll-up-button';
import SocialMediaIcons from 'react-social-media-icons';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4),
    textAlign: 'center',
    backgroundColor: theme.palette.primary.main,
    color: 'white',
  },
  logo: {
    width: 50,
  },
}));

const socialMediaIcons = [
  {
    className: 'fa-youtube-square',
    url: 'https://www.youtube.com/channel/UC29dGuu_SEw_hYuAfNYKGGg',
  },
  {
    className: 'fa-twitter',
    url: 'https://twitter.com/ChildNth',
  },
];

const Footer: React.FC = () => {
  const classes = useStyles();
  return (
    <footer className={classes.root}>
      <Grid container justify="center" alignItems="center" direction="column">
        <Grid item>
          <SocialMediaIcons
            icons={socialMediaIcons}
            iconSize={'1.5em'}
            iconColor={'#fff'}
          />
        </Grid>
      </Grid>
      <ScrollUpButton />
    </footer>
  );
};

export default withRouter(Footer);
