import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles(theme => ({
  header: {
    padding: theme.spacing(2),
    backgroundColor: theme.palette.primary.light,
  },
  paper: {
    margin: theme.spacing(),
  },
}));

interface Props {
  title: string;
  children: React.ReactChild;
}

const SimplePaper: React.FC<Props> = ({ title, children }) => {
  const classes = useStyles();
  return (
    <Paper className={classes.paper} elevation={6}>
      <Typography variant="h6" className={classes.header}>
        <Box fontWeight="fontWeightLight">{title}</Box>
      </Typography>
      <div>{children}</div>
    </Paper>
  );
};

export default SimplePaper;
