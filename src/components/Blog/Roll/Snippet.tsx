import React from 'react';
import { withRouter } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ShareIcon from '@material-ui/icons/Share';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  card: {
    margin: theme.spacing(),
  },
  avatar: {
    backgroundColor: theme.palette.primary.main,
  },
  video: {
    marginTop: theme.spacing(5),
    margin: theme.spacing(1),
  },
  videoIframe: {
    width: 560,
    height: 315,
    [theme.breakpoints.down('sm')]: {
      width: 320,
      height: 280,
    },
  },
  title: {
    cursor: 'pointer',
  },
}));

const Snippet = ({
  letter,
  slug,
  title,
  video,
  createdAt,
  description,
  history,
}: any) => {
  const classes = useStyles();
  return (
    <Card className={classes.card} elevation={6}>
      <CardHeader
        avatar={<Avatar className={classes.avatar}>{letter}</Avatar>}
        title={title}
        subheader={new Date(createdAt).toDateString()}
        onClick={() => {
          history.push(`/post/${slug}`);
        }}
        classes={{
          title: classes.title,
        }}
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="div">
          <Box fontWeight="fontWeightMedium" m={1}>
            {description}
          </Box>
        </Typography>
        {video && (
          <div className={classes.video}>
            <iframe
              className={classes.videoIframe}
              src={video}
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
              title="nthchild consulting Video"
            ></iframe>
          </div>
        )}
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <Button
          color="primary"
          onClick={() => {
            history.push(`/post/${slug}`);
          }}
        >
          Read Article
        </Button>
      </CardActions>
    </Card>
  );
};

export default withRouter(Snippet);
