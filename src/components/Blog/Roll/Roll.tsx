import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Snippet from './Snippet';
import * as Firebase from 'services/Firebase';
import { useStore } from 'components/App/store/Context';

const Roll: React.FC = ({ match }: any) => {
  const [posts, setPosts] = useState<any>([]);
  const { user }: any = useStore();
  useEffect(() => {
    if (user) {
      const fetchPosts = async () => {
        if (match.params.tag) {
          const results = await Firebase.getPostsByTag(match.params.tag);
          setPosts(results);
        } else {
          const results = await Firebase.getPosts();
          setPosts(results);
        }
      };
      fetchPosts();
    }
  }, [user, match.params.tag]);
  return (
    <Grid container justify="center">
      {posts.map((post: any, i: number) => {
        return (
          <Grid item key={i} xs={12}>
            <Snippet
              letter={post.title[0]}
              slug={post.slug}
              title={post.title}
              video={post.video}
              createdAt={post.createdAt}
              description={post.description}
            />
          </Grid>
        );
      })}
    </Grid>
  );
};

export default withRouter(Roll);
