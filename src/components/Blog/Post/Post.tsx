import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import * as Firebase from 'services/Firebase';
import Parser from 'html-react-parser';
import unescape from 'lodash/unescape';
import Chip from '@material-ui/core/Chip';
import { Helmet } from 'react-helmet';
import Divider from '@material-ui/core/Divider';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(5),
    marginTop: theme.spacing(10),
  },
  margin: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  chip: {
    margin: theme.spacing(1),
  },
  video: {
    marginTop: theme.spacing(5),
  },
  header: {
    padding: theme.spacing(2),
  },
  videoIframe: {
    width: 560,
    height: 315,
    [theme.breakpoints.down('sm')]: {
      width: 320,
      height: 280,
    },
  },
}));

const Post = ({ match, history }: any) => {
  const classes = useStyles();
  const [post, setPost] = useState<any>({});
  useEffect(() => {
    Firebase.listenForAuth((user: any) => {
      if (user) {
        const fetchPost = async () => {
          if (match.params.slug) {
            const results = await Firebase.getPost(match.params.slug);
            setPost(results);
          }
        };
        fetchPost();
      }
    });
  }, [match.params.slug]);
  if (!post || !post.id) {
    return <div />;
  }
  return (
    <>
      <Helmet>
        <title>nthchild consulting - {post.title}</title>
        <meta name="description" content={post.description} />
      </Helmet>
      <Container fixed className={classes.root}>
        <Typography variant="h2" component="div" className={classes.margin}>
          {post.title}
        </Typography>
        <Typography variant="subtitle1" color="textSecondary" component="div">
          <Box fontWeight="fontWeightMedium" marginTop={3}>
            {post.description}
          </Box>
        </Typography>
        <Typography variant="subtitle1" color="textSecondary" component="div">
          <Box fontWeight="fontWeightLight" marginTop={2}>
            {new Date(post.createdAt).toDateString()}
          </Box>
        </Typography>
        <Box marginTop={3} marginBottom={3}>
          <Divider />
        </Box>
        {post.video && (
          <div className={classes.video}>
            <iframe
              className={classes.videoIframe}
              src={post.video}
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
              title="nthchild consulting Video"
            ></iframe>
          </div>
        )}
        <Typography component="div" variant="subtitle1">
          <Box fontWeight="fontWeightLight" marginTop={2}>
            {Parser(unescape(post.post))}
          </Box>
        </Typography>
        <Divider />
        <Typography variant="h6" className={classes.header}>
          <Box fontWeight="fontWeightLight">Tags:</Box>
        </Typography>
        {post.tags.map((v: any, i: number) => (
          <Chip
            key={i}
            label={v}
            clickable
            className={classes.chip}
            onClick={() => {
              history.push(`/tag/${v}`);
            }}
          />
        ))}
      </Container>
    </>
  );
};

export default withRouter(Post);
