import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Slide from '@material-ui/core/Slide';
import Logo from 'components/Logo/Logo';

const ShowOnScroll = (props: any) => {
  const { children, window } = props;
  const trigger = useScrollTrigger({ target: window ? window() : undefined });
  return (
    <Slide appear={props.appear || false} direction="down" in={trigger}>
      {children}
    </Slide>
  );
};

const SlideDownAppBar = (props: any) => {
  if (props.static) {
    return (
      <AppBar color="secondary">
        <Toolbar>
          <Logo light="true" />
        </Toolbar>
      </AppBar>
    );
  }
  return (
    <>
      <ShowOnScroll {...props}>
        <AppBar color="secondary">
          <Toolbar>
            <Logo light="true" />
          </Toolbar>
        </AppBar>
      </ShowOnScroll>
    </>
  );
};

export default SlideDownAppBar;
