import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import IconButton from '@material-ui/core/IconButton';
import Scroll from 'react-scroll';
import Box from '@material-ui/core/Box';
import Logo from 'components/Logo/Logo';

const scroller = Scroll.scroller;

const useStyles = makeStyles(theme => ({
  root: {
    height: '100vh',
  },
  container: {
    height: '100vh',
  },
  logo: {
    width: 50,
  },
  button: {
    margin: theme.spacing(1),
  },
}));

const Header: React.FC = () => {
  const classes = useStyles();
  return (
    <header className={classes.root}>
      <Grid
        container
        justify="center"
        alignItems="center"
        direction="column"
        className={classes.container}
      >
        <Grid item>
          <Grid container justify="center" alignItems="center">
            <Logo justify="center" />
          </Grid>
        </Grid>
        <Grid item>
          <Grid container justify="center" alignItems="center">
            <Box m={4}>
              <Typography variant="caption" color="textSecondary" component="p">
                A tech blog for self starters
              </Typography>
            </Box>
          </Grid>
        </Grid>
        <Grid item>
          <IconButton
            color="primary"
            className={classes.button}
            component="span"
            onClick={() => {
              scroller.scrollTo('blog', {
                duration: 500,
                smooth: true,
              });
            }}
          >
            <KeyboardArrowDownIcon fontSize="large" />
          </IconButton>
        </Grid>
      </Grid>
    </header>
  );
};

export default Header;
