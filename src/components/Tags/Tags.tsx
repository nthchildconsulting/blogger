import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import SimplePaper from 'components/SimplePaper/SimplePaper';
import Chip from '@material-ui/core/Chip';
import * as Firebase from 'services/Firebase';
import { useStore } from 'components/App/store/Context';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(),
  },
  chip: {
    margin: theme.spacing(1),
  },
}));

const Tags: React.FC = ({ history }: any) => {
  const classes = useStyles();
  const [tags, setTags] = useState<any>([]);
  const { user }: any = useStore();
  useEffect(() => {
    if (user) {
      const fetchTags = async () => {
        const results = await Firebase.getTags();
        setTags(results);
      };
      fetchTags();
    }
  }, [user]);
  return (
    <SimplePaper title="Tags">
      <div className={classes.root}>
        {tags.map((v: any, i: number) => (
          <Chip
            key={i}
            label={v}
            clickable
            className={classes.chip}
            onClick={() => {
              history.push(`/tag/${v}`);
            }}
          />
        ))}
      </div>
    </SimplePaper>
  );
};

export default withRouter(Tags);
