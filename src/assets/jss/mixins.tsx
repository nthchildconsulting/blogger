export const fixedBackground = (background: string) => {
  return {
    backgroundImage: `linear-gradient(rgba(211,211,211,0.2),rgba(211,211,211,0.2)), url(${background})`,
    backgroundAttachment: 'fixed',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  };
};
