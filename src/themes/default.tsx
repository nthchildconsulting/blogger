import { createMuiTheme } from '@material-ui/core/styles';
import grey from '@material-ui/core/colors/grey';

const theme = createMuiTheme({
  palette: {
    type: 'light',
    primary: {
      main: grey[900],
      light: grey[300],
    },
    secondary: {
      main: grey[900],
    },
  },
  typography: {
    subtitle1: {
      fontSize: 24,
      fontWeight: 'normal',
    },
  },
});

export default theme;
