import React, { useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import AppBar from 'components/AppBar/AppBar';
import Footer from 'components/Footer/Footer';
import PopularPosts from 'components/PopularPosts/PopularPosts';
import Tags from 'components/Tags/Tags';
import BlogRoll from 'components/Blog/Roll/Roll';
import Typography from '@material-ui/core/Typography';
import { Helmet } from 'react-helmet';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(),
    minHeight: '100vh',
  },
  container: {
    paddingTop: theme.spacing(10),
  },
  tag: {
    padding: theme.spacing(2),
  },
}));

const Posts: React.FC = ({ match }: any) => {
  const classes = useStyles();
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <>
      <Helmet>
        <title>nthchild consulting</title>
      </Helmet>
      <AppBar static={true} />
      <div className={classes.container}>
        <Typography className={classes.tag} variant="h5">
          Tag: {match.params.tag}
        </Typography>
        <Grid container className={classes.root}>
          <Grid item xs={12} sm={7} md={8}>
            <BlogRoll />
          </Grid>
          <Grid item xs={12} sm={5} md={4}>
            <Grid container direction="column">
              <Grid item>
                <PopularPosts />
              </Grid>
              <Grid item>
                <Tags />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
      <Footer />
    </>
  );
};

export default withRouter(Posts);
