import React, { useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import { useStore } from '../store/Context';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { withSnackbar } from 'notistack';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    width: '100%',
    height: '100%',
  },
  paper: {
    width: 400,
    height: 350,
    textAlign: 'center',
    padding: theme.spacing(2),
  },
  button: {
    margin: theme.spacing(4),
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
}));

interface Props {
  enqueueSnackbar: any;
}

const Login: React.FC<Props> = ({ enqueueSnackbar }) => {
  const classes = useStyles();
  const { login }: any = useStore();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const onLogin = async (e: any) => {
    e.preventDefault();
    try {
      await login(email, password);
      enqueueSnackbar('You have successfully logged in.', {
        variant: 'success',
      });
    } catch (e) {
      enqueueSnackbar(e.message, { variant: 'error' });
    }
  };
  return (
    <Grid
      container
      justify="center"
      alignContent="center"
      className={classes.root}
    >
      <Paper className={classes.paper} elevation={6}>
        <form onSubmit={onLogin}>
          <Box component="span" p={10}>
            <Typography variant="h4">nthchild consulting</Typography>
          </Box>
          <div>
            <TextField
              id="email"
              label="Email"
              className={classes.textField}
              value={email}
              onChange={e => setEmail(e.target.value)}
              margin="normal"
            />
            <TextField
              id="password"
              label="Password"
              className={classes.textField}
              type="password"
              autoComplete="current-password"
              onChange={e => setPassword(e.target.value)}
              margin="normal"
            />
          </div>
          <Button
            type="submit"
            className={classes.button}
            variant="contained"
            color="primary"
            onClick={onLogin}
          >
            Login
          </Button>
        </form>
      </Paper>
    </Grid>
  );
};

export default withSnackbar(Login);
