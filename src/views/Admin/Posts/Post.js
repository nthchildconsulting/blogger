import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import SimplePaper from 'components/SimplePaper/SimplePaper';
import Switch from '@material-ui/core/Switch';
import Draft from 'components/Draft/Draft2';
import { stateToHTML } from 'draft-js-export-html';
import kebabCase from 'lodash/kebabCase';
import escape from 'lodash/escape';
import unescape from 'lodash/unescape';
import map from 'lodash/map';
import trim from 'lodash/trim';
import * as Firebase from 'services/Firebase';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { EditorState, convertFromHTML, ContentState } from 'draft-js';
import { withSnackbar } from 'notistack';

const useStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  button: {
    margin: theme.spacing(),
  },
  editorContainer: {
    padding: theme.spacing(),
  },
  editor: {
    border: '1px solid lightgrey',
    borderRadius: '5px',
    padding: theme.spacing(),
  },
}));

const initialState = {
  slug: '',
  title: '',
  video: '',
  tags: '',
  popular: false,
  description: '',
  post: '',
  pinned: false,
};

const Post = ({ history, match, enqueueSnackbar }) => {
  const classes = useStyles();
  const [state, setState] = useState({ ...initialState });
  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const onSubmit = async e => {
    e.preventDefault();
    const trimmedTags = map(state.tags.split(','), trim);
    const options = {
      ...state,
      tags: trimmedTags,
      post: escape(state.post),
    };
    await Firebase.savePost(options);
    enqueueSnackbar('You have successfully saved the post.', {
      variant: 'success',
    });
  };
  const canSubmit =
    state.title && state.tags && state.description && state.post;

  useEffect(() => {
    if (match.params.id) {
      const fetchPost = async id => {
        const results = await Firebase.getPost(id);
        if (results) {
          setState({
            ...results,
            tags: results.tags.join(', '),
          });
          const blocksFromHTML = convertFromHTML(unescape(results.post));
          const data = ContentState.createFromBlockArray(
            blocksFromHTML.contentBlocks,
            blocksFromHTML.entityMap
          );
          setEditorState(EditorState.createWithContent(data));
        }
      };
      fetchPost(match.params.id);
    } else {
      const reset = async () => {
        await setEditorState(EditorState.createEmpty());
        await setState({ ...initialState });
      };
      reset();
    }
  }, [match.params.id]);

  const onHandleChange = (name, value) => {
    if (name === 'title') {
      setState({ ...state, slug: kebabCase(value), title: value });
    } else {
      setState({ ...state, [name]: value });
    }
  };

  return (
    <SimplePaper title="Post">
      <div className={classes.root}>
        <div>
          <TextField
            id="slug"
            label="Slug"
            className={classes.textField}
            value={state.slug}
            margin="normal"
            fullWidth
            disabled
          />
        </div>
        <div>
          <TextField
            id="title"
            label="Title"
            className={classes.textField}
            value={state.title}
            onChange={e => onHandleChange('title', e.target.value)}
            margin="normal"
            fullWidth
          />
        </div>
        <div>
          <TextField
            id="video"
            label="Video"
            className={classes.textField}
            value={state.video}
            onChange={e => onHandleChange('video', e.target.value)}
            margin="normal"
            fullWidth
          />
        </div>
        <div>
          <TextField
            id="tags"
            label="Tags"
            className={classes.textField}
            value={state.tags}
            onChange={e => onHandleChange('tags', e.target.value)}
            margin="normal"
            fullWidth
          />
        </div>
        <div>
          <FormGroup row>
            <FormControlLabel
              control={
                <Switch
                  checked={state.popular}
                  className={classes.textField}
                  onChange={e => onHandleChange('popular', e.target.checked)}
                  color="primary"
                />
              }
              label="Popular Post"
            />
          </FormGroup>
        </div>
        <div>
          <FormGroup row>
            <FormControlLabel
              control={
                <Switch
                  checked={state.pinned}
                  className={classes.textField}
                  onChange={e => onHandleChange('pinned', e.target.checked)}
                  color="primary"
                />
              }
              label="Pinned"
            />
          </FormGroup>
        </div>
        <div>
          <TextField
            id="description"
            label="Description"
            className={classes.textField}
            value={state.description}
            onChange={e => onHandleChange('description', e.target.value)}
            margin="normal"
            multiline
            rows="3"
            variant="outlined"
            fullWidth
          />
        </div>
        <div className={classes.editorContainer}>
          <div className={classes.editor}>
            <Draft
              onChange={editorState => {
                const html = stateToHTML(editorState.getCurrentContent());
                onHandleChange('post', html);
                setEditorState(editorState);
              }}
              editorState={editorState}
            />
          </div>
        </div>
        <Button
          type="submit"
          className={classes.button}
          variant="contained"
          color="primary"
          onClick={onSubmit}
          disabled={!canSubmit}
        >
          Submit
        </Button>
      </div>
    </SimplePaper>
  );
};

export default withRouter(withSnackbar(Post));
