import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import SimplePaper from 'components/SimplePaper/SimplePaper';
import * as Firebase from 'services/Firebase';
import Switch from '@material-ui/core/Switch';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { withSnackbar } from 'notistack';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import ConfirmDialog from 'components/Dialogs/Confirm/Confirm';

const Post = withSnackbar(({ post, enqueueSnackbar, update }) => {
  const [confirmOpen, setConfirmOpen] = useState(false);
  const publishPost = async publish => {
    await Firebase.publishPost(post.id, publish);
    if (publish) {
      enqueueSnackbar('You have successfully published the post');
    } else {
      enqueueSnackbar('You have successfully unpublished the post');
    }
  };
  const deletePost = async () => {
    await Firebase.deletePost(post.id);
    enqueueSnackbar('You have successfully deleted the post');
    update();
  };
  return (
    <TableRow>
      <TableCell component="th" scope="row">
        <Link to={`/admin/posts/${post.id}/edit`}>{post.title}</Link>
      </TableCell>
      <TableCell align="right">{post.createdAt}</TableCell>
      <TableCell>
        <FormGroup row>
          <FormControlLabel
            control={
              <Switch
                defaultChecked={post.publish || false}
                onChange={e => {
                  publishPost(e.target.checked);
                }}
                color="primary"
              />
            }
            label="Publish"
          />
        </FormGroup>
      </TableCell>
      <TableCell>
        <IconButton aria-label="delete" onClick={() => setConfirmOpen(true)}>
          <DeleteIcon />
        </IconButton>
        <ConfirmDialog
          title="Delete Post?"
          open={confirmOpen}
          setOpen={setConfirmOpen}
          onConfirm={deletePost}
        >
          Are you sure you want to delete this post?
        </ConfirmDialog>
      </TableCell>
    </TableRow>
  );
});

const Posts = () => {
  const [posts, setPosts] = useState([]);
  const [forceUpdate, setForceUpdate] = useState(false);
  const fetchPosts = async () => {
    const results = await Firebase.getAdminPosts();
    setPosts(results);
  };
  const update = () => {
    setForceUpdate(!forceUpdate);
  };
  useEffect(() => {
    fetchPosts();
  }, [forceUpdate]);
  return (
    <SimplePaper title="Posts">
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Title</TableCell>
            <TableCell align="right">Created At</TableCell>
            <TableCell></TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {posts.map(post => (
            <Post post={post} key={post.id} update={update} />
          ))}
        </TableBody>
      </Table>
    </SimplePaper>
  );
};

export default Posts;
