import React, { useState, useContext, useEffect } from 'react';
import * as Firebase from 'services/Firebase';

const Context = React.createContext([{}, () => {}]);

const Provider = props => {
  const [user, setUser] = useState(null);
  const [showLogin, setShowLogin] = useState(false);

  const login = (email, password) => {
    return Firebase.adminLogin(email, password);
  };
  const logout = async () => {
    await Firebase.logout();
    window.location.href = '/admin';
  };

  useEffect(() => {
    Firebase.listenForAuth(user => {
      if (user && !user.isAnonymous) {
        setUser(user);
      } else {
        setShowLogin(true);
      }
    });
  }, []);

  return (
    <Context.Provider
      value={{
        showLogin,
        login,
        logout,
        user,
      }}
    >
      {props.children}
    </Context.Provider>
  );
};

export const useStore = () => useContext(Context);

export function withProvider(Component) {
  return function WrapperComponent(props) {
    return (
      <Provider>
        <Component {...props} />
      </Provider>
    );
  };
}

export { Context, Provider };
