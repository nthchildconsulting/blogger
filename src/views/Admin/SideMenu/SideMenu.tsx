import React from 'react';
import { withRouter } from 'react-router-dom';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListIcon from '@material-ui/icons/List';
import PostAddIcon from '@material-ui/icons/PlaylistAdd';
import LogoutIcon from '@material-ui/icons/ExitToApp';
import Divider from '@material-ui/core/Divider';
import SimplePaper from 'components/SimplePaper/SimplePaper';
import { useStore } from '../store/Context';

interface Props {
  history: any;
}

const SideMenu: React.FC<Props> = ({ history }) => {
  const { logout }: any = useStore();
  return (
    <SimplePaper title="Admin Menu">
      <List>
        <ListItem button onClick={() => history.push(`/admin/posts`)}>
          <ListItemIcon>
            <ListIcon />
          </ListItemIcon>
          <ListItemText primary="Posts" />
        </ListItem>
        <Divider />
        <ListItem button onClick={() => history.push(`/admin/posts/create`)}>
          <ListItemIcon>
            <PostAddIcon />
          </ListItemIcon>
          <ListItemText primary="New Post" />
        </ListItem>
        <Divider />
        <ListItem button onClick={logout}>
          <ListItemIcon>
            <LogoutIcon />
          </ListItemIcon>
          <ListItemText primary="Logout" />
        </ListItem>
      </List>
    </SimplePaper>
  );
};

export default withRouter(SideMenu);
