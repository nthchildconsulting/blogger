import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { withProvider, useStore } from './store/Context';
import Login from './Login/Login';
import { SnackbarProvider } from 'notistack';
import Grid from '@material-ui/core/Grid';
import SideMenu from './SideMenu/SideMenu';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import Posts from './Posts/Posts';
import Post from './Posts/Post';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(),
  },
}));

const Admin = withRouter(() => {
  const classes = useStyles();
  const { user, showLogin }: any = useStore();
  if (!user && !showLogin) {
    return <div />;
  }
  if (!user) {
    return <Login />;
  }
  return (
    <Grid container className={classes.root}>
      <Grid item xs={12} sm={12} md={4}>
        <SideMenu />
      </Grid>
      <Grid item xs={12} sm={12} md={8}>
        <Switch>
          <Route path="/admin/posts" exact component={Posts} />
          <Route path="/admin/posts/create" exact component={Post} />
          <Route path="/admin/posts/:id/edit" exact component={Post} />
          <Redirect exact from="/admin" to="/admin/posts" />
        </Switch>
      </Grid>
    </Grid>
  );
});

const App: React.FC = () => {
  return (
    <SnackbarProvider>
      <Admin />
    </SnackbarProvider>
  );
};

export default withProvider(App);
