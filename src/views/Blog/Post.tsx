import React from 'react';
import AppBar from 'components/AppBar/AppBar';
import Footer from 'components/Footer/Footer';
import BlogPost from 'components/Blog/Post/Post';

const Post: React.FC = () => {
  return (
    <>
      <AppBar static={true} />
      <BlogPost />
      <Footer />
    </>
  );
};

export default Post;
