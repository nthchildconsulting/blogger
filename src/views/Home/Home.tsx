import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Scroll from 'react-scroll';
import AppBar from 'components/AppBar/AppBar';
import Header from 'components/Header/Header';
import Footer from 'components/Footer/Footer';
import PopularPosts from 'components/PopularPosts/PopularPosts';
import Tags from 'components/Tags/Tags';
import BlogRoll from 'components/Blog/Roll/Roll';
import { Helmet } from 'react-helmet';
import Parallax from 'components/Parallax/Parallax';
import background from 'assets/img/coffee.jpg';
import tiledBackground from 'assets/img/tiled-background.png';

const Element = Scroll.Element;

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(),
    minHeight: '100vh',
    paddingTop: theme.spacing(10),
  },
  tiledBackground: {
    backgroundImage: `url(${tiledBackground})`,
    backgroundPosition: 'center',
    backgroundRepeat: 'repeat',
    backgroundSize: 'contain',
    padding: theme.spacing(5),
    paddingTop: theme.spacing(),
    paddingBottom: theme.spacing(10),
    [theme.breakpoints.down('sm')]: {
      paddingLeft: 0,
      paddingRight: 0,
    },
  },
}));

const Home: React.FC = () => {
  const classes = useStyles();
  return (
    <>
      <Helmet>
        <title>nthchild consulting</title>
      </Helmet>
      <AppBar />
      <Header />
      <Parallax background={background} />
      <Element name="blog">
        <div className={classes.tiledBackground}>
          <Grid container className={classes.root}>
            <Grid item xs={12} sm={7} md={8}>
              <BlogRoll />
            </Grid>
            <Grid item xs={12} sm={5} md={4}>
              <Grid container direction="column">
                <Grid item>
                  <PopularPosts />
                </Grid>
                <Grid item>
                  <Tags />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </div>
      </Element>
      <Footer />
    </>
  );
};

export default Home;
